Feature: Comprar un producto

  Background:

    Given Acceder a la pagina "https://petstore.octoperf.com/"

  Scenario: Iniciar sesion y realizar la compra de un producto
    Given click boton "Enter the Store"
    And click boton "SingIn"
    And login "usuario Micha"
    And login "password 12345"
    And dar click en boton login
    And click al boton "Dogs"
    And click al boton "K9-BD-01"
    And click al boton "add to cart"
    And click al boton "proceed to checkout"
    And click al boton "continue"
    When click al boton "confirm"
    Then validar mensaje "Thank you, your order has been submitted."

    # En este escenario cuando abre la pagina sale un mensaje de chrome que dice "puedes abrir los favoritos, el modo
    # de lectura y mas desde el menu de chrome" y se rompe el escenario, cerrando el mensaje de manera manual y dando click al boton
    # "enter the store" la prueba continua sin problemas.