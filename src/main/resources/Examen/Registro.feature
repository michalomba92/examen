Feature: Registro de Usuario
  
  Background: 
    
    Given Acceder a la pagina "https://petstore.octoperf.com/"

    Scenario: Registrar un nuevo Usuario
      Given click boton "EnterTheStore"
      And click boton "SingIn"
      And click boton RegisterNow
      And completar formulario
        | UserId | NewPassword | RepeatPassword | FirstName | LastName | Email           | Phone     | Adress1 | Adress2 | City       | State      | Zip   | Country |
        | Micha  | 12345       | 12345          | Michael   | Lombardi | micha@gmail.com | 091333333 | Artigas | Soriano | Montevideo | Montevideo | 11200 | Uruguay |
      When click en boton "SaveInformation"