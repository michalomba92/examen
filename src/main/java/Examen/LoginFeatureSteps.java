package Examen;

import io.cucumber.java.en.And;
import io.cucumber.java.en.When;

public class LoginFeatureSteps {
    Sistema sist = new Sistema();
    LoginLocalizadores log = new LoginLocalizadores();

    @And("login {string}")
    public void login(String boton) {
        switch (boton){
            case "usuario Micha":
                sist.clickElement(log.user);
                sist.escribir(log.user, "Micha");
                break;
            case "password 12345":
                sist.clickElement(log.password);
                sist.escribir(log.password, "12345");
                break;
        }
    }

    @When("dar click en boton login")
    public void darClickEnBotonLogin() {
        sist.clickElement(log.botonLogin);
    }
}
