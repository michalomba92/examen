package Examen;

public class RegistroLocalizadores {

    //Formulario

    String register = "//div[@id='Catalog']/a"; // boton registro

    String usuario = "//div[@id='Catalog']/form/table/tbody/tr/td[2]/input"; // campo de usuario

    String password = "//div[@id='Catalog']/form/table/tbody/tr[2]/td[2]/input"; // contraseña

    String rpassword = "//div[@id='Catalog']/form/table/tbody/tr[3]/td[2]/input"; // repetir contraseña

    String fname = "//div[@id='Catalog']/form/table[2]/tbody/tr/td[2]/input"; // // campo de nombre

    String lname = "//div[@id='Catalog']/form/table[2]/tbody/tr[2]/td[2]/input"; // // campo de apeliido

    String email = "//div[@id='Catalog']/form/table[2]/tbody/tr[3]/td[2]/input"; // // campo de email

    String phone = "//div[@id='Catalog']/form/table[2]/tbody/tr[4]/td[2]/input"; // campo de telefono

    String addres1 = "//div[@id='Catalog']/form/table[2]/tbody/tr[5]/td[2]/input"; // campo de direccion 1

    String addres2 = "//div[@id='Catalog']/form/table[2]/tbody/tr[6]/td[2]/input"; // campo de direccion 2

    String city = "//div[@id='Catalog']/form/table[2]/tbody/tr[7]/td[2]/input"; // campo de ciudad

    String state = "//div[@id='Catalog']/form/table[2]/tbody/tr[8]/td[2]/input"; // campo de estado

    String zip = "//div[@id='Catalog']/form/table[2]/tbody/tr[9]/td[2]/input"; // campo de codigo postal

    String country = "//div[@id='Catalog']/form/table[2]/tbody/tr[10]/td[2]/input";// campo de pais

    String save = "//div[@id='Catalog']/form/input"; // boton guardar
}
