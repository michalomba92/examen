package Examen;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;


import java.util.List;
import java.util.Map;

import static Examen.Sistema.driver;

public class RegistroFeatureStep {
    Sistema sist = new Sistema();
    HomeMenu menu = new HomeMenu();
    RegistroLocalizadores reg = new RegistroLocalizadores();

    @Given("Acceder a la pagina {string}")
    public void accederALaPagina(String url) {
        driver.get(url);
        driver.manage().window().maximize();
    }


    @Given("click boton {string}")
    public void clickBoton(String boton) {
        switch (boton) {
            case "EnterTheStore":
                sist.clickElement(menu.entrar);
                break;
            case "SingIn":
                sist.clickElement(menu.signIn);
                break;
        }
    }

    @And("click boton RegisterNow")
    public void clickBotonRegisterNow() {
        sist.clickElement(reg.register);
    }

    @And("completar formulario")
    public void completarFormulario(DataTable dataTable) {
        List<Map<String, String>> tabla = dataTable.asMaps(String.class, String.class);
        for (Map<String, String> data:tabla){
            sist.escribir(reg.usuario, data.get("UserId"));
            sist.escribir(reg.password, data.get("NewPassword"));
            sist.escribir(reg.rpassword, data.get("RepeatPassword"));
            sist.escribir(reg.fname, data.get("FirstName"));
            sist.escribir(reg.lname, data.get("LastName"));
            sist.escribir(reg.email, data.get("Email"));
            sist.escribir(reg.phone, data.get("Phone"));
            sist.escribir(reg.addres1, data.get("Adress1"));
            sist.escribir(reg.addres2, data.get("Adress2"));
            sist.escribir(reg.city, data.get("City"));
            sist.escribir(reg.state, data.get("State"));
            sist.escribir(reg.zip, data.get("Zip"));
            sist.escribir(reg.country, data.get("Country"));
        }
    }

    @When("click en boton {string}")
    public void clickEnBoton(String Boton) {
        sist.clickElement(reg.save);
    }
}
