package Examen;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.openqa.selenium.By;
import org.testng.Assert;


public class CompraFeatureSteps {
    Sistema sist = new Sistema();
    HomeMenu log = new HomeMenu();
    CompraPerroLocalizadores cp = new CompraPerroLocalizadores();
    CheckoutLocalizadores ch = new CheckoutLocalizadores();

    @And("click al boton {string}")
    public void clickAlBoton(String boton) {
        switch (boton) {
            case "Dogs":
                sist.clickElement(log.catDog);
                break;
            case "K9-BD-01":
                sist.clickElement(cp.bulldog);
                break;
            case "add to cart":
                sist.clickElement(cp.addMbulldog);
                break;
            case "proceed to checkout":
                sist.clickElement(ch.pCheckOut);
                break;
            case "continue":
                sist.clickElement(ch.continuar);
                break;
            case "confirm":
                sist.clickElement(ch.confrim);
        }
    }

    @Then("validar mensaje {string}")
    public void validarMensaje(String message) {
        Assert.assertEquals(sist.obtenerTexto(ch.textoCompra), message);
    }
}
